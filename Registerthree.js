/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage,
  StyleSheet,
  Button,
  KeyboardAvoidingView,
  Image,
  Navigator,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const ACCESS_TOKEN = 'access_token';

class Register extends Component {
  state = {
    age: "",
    gender: "",
    agebool: false,
    genderbool: false
  };

  onSegmentChange = (segment: String) => {
    this.setState({behavior: segment.toLowerCase()});
    return false;
  };

  onForgotPass = () => {
    this.setState({secure:!this.state.secure}, function(){
      if (this.state.secure)
        alert('Your pass is now hidden');
      else
        alert('Your pass is now visible. Be careful.');
    });
    return false;
  }

  enableNext = () => {
    if (this.state.emailbool && this.state.passwordbool) {
      this.setState({next: '#fff'});
    }else{
      this.setState({next: '#6f6f6f'});
    }
    return false;
  }

 checkPassToRegisterLogin = () => {
    try {
      let response = fetch('http://skywegapp-aws.us-west-2.elasticbeanstalk.com/users', {
                              method: 'POST',
                              headers:{
                                "content-type": "application/json"/*,
                                "authorization": "e87a312cff5884b31266aed8d4e17d96588bec9e",
                                "cache-control": "no-cache"*/
                              },
                              body: JSON.stringify({
                               "email": this.props.data.email,
                               "username": this.props.data.username,
                               "firstname": this.props.data.firstname,
                               "lastname": this.props.data.lastname,
                               "password": this.props.data.password
                              /* "gender": this.state.gender,
                               "age": this.state.age*/ 
                              })
                            }).then((response) => {
                              console.warn('éxito!!');
                              console.warn(JSON.stringify(response)); console.warn(response); }).catch((error) => { console.warn('error'); console.warn(error); throw error; }); let res = response; } catch(error) { console.warn(error); console.warn('hola'); } } /* checkPassToRegisterLogin = () => {
    let variable = [this.props.data.email, this.props.data.username, this.props.data.firstname,
      this.props.data.lastname, this.props.data.password, this.state.gender, this.state.age];
    try {
      let response = fetch('http://testskyweg-dev.us-west-2.elasticbeanstalk.com/users', {
                              method: 'POST',
                              headers:{
                                "content-type": "application/json",
                                "authorization": "Token d57f5aaae686dc366d3364b4b337c7f6d1b774f5",
                                "cache-control": "no-cache"
                              },
                              body: JSON.stringify({
                               "email": this.props.data.email,
                               "username": this.props.data.username,
                               "firstname": this.props.data.firstname,
                               "lastname": this.props.data.lastname,
                               "password": this.props.data.password,
                               "gender": this.state.gender,
                               "age": this.state.age
                              })
                            });
      let res = response;
      console.warn('exito');
      console.warn(variable);
      console.warn(JSON.stringify(res));

    } catch(errors) {
      console.warn(errors);
      console.warn('hola');
    }

  }
*/
  render() {
    return (
      <Navigator
        renderScene={this.renderScene.bind(this)}
        navigator={this.props.navigator}
      />
    )
  }

  renderScene(route, navigator) {
    return (
      <View style={styles.homeOne}>
        <LinearGradient colors={['#252EC2', '#EA5030']} style={styles.loginStyles_}>
          <View style={{flex:1}}>
            <View style={{flex:1, flexDirection:'row', justifyContent:'space-around', alignItems: 'center'}}>
              <Text style={{color:'#FFF'}}>Back</Text>
              <Text style={{color:'#FFF'}}>Create account</Text>
              <Text style={{color: this.state.next }} onPress={this.checkPassToRegisterLogin}>Next</Text>
            </View>
            <View style={{flex:1, flexDirection: 'row', justifyContent:'center'}}>
              <TextInput
                onChangeText={
                  (text)=>{
                    this.setState({email: text}, function(){ this.setState({emailbool:(this.state.age == '' ? false : true)}, this.enableNext);
                  });
                }}
                style={{marginBottom:1, borderWidth: 0, width: 250, backgroundColor:'#FFF', opacity:0.3, height:35}}
                placeholder="         Age"
                autoFocus={true}
                underlineColorAndroid='rgba(0,0,0,0)'
              />
            </View>
            <View style={{flex:4, flexDirection: 'row', justifyContent:'center'}}>
              <TextInput
                onChangeText={
                  (text)=>{
                    this.setState({password: text}, function(){ this.setState({passwordbool:(this.state.gender == '' ? false : true)}, this.enableNext);
                  });
                }}
                style={{marginBottom:1, borderWidth: 0, width: 250, backgroundColor:'#FFF', opacity:0.3, height:35}}
                placeholder="         Gender"
                underlineColorAndroid='rgba(0,0,0,0)'
              />
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  homeOne:{
    flex:1,
    flexDirection:'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start'
  },

  loginStyles_: {
    flex:1,
    paddingTop: 10
  },

  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});

module.exports = Register;
