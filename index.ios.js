/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

 import React, { Component } from 'react';
 import {
   AppRegistry,
   StyleSheet,
   Button,
   KeyboardAvoidingView,
   Image,
   Navigator,
   Text,
   TextInput,
   TouchableHighlight,
   View
 } from 'react-native';
 import LinearGradient from 'react-native-linear-gradient';

 var Login = require('./Login.js');
 var Register = require('./Register.js');
 var Registerone = require('./Registerone.js');
 var Registertwo = require('./Registertwo.js');
 var Registerthree = require('./Registerthree.js');

 class Skyweg extends Component {
  render() {
    return (
      <Navigator
        initialRoute={{id: 'Login', name:'Index'}}
        renderScene={this.renderScene.bind(this)}
      />
    );
  }

  renderScene(route, navigator) {
    var routedId = route.id;
    if(routedId === 'Login') {
      return (
        <Login
          navigator={navigator}/>
      )
    }else if(routedId === 'Register') {
      return (
        <Register
          navigator={navigator} data={route.passProps}  route={route}/>
      );
    }else if(routedId === 'Registerone') {
      return (
        <Registerone
          navigator={navigator} data={route.passProps} route={route}/>
      );
    }else if(routedId === 'Registertwo') {
      return (
        <Registertwo
          navigator={navigator} data={route.passProps} route={route}/>
      );
    }else if(routedId === 'Registerthree') {
      return (
        <Registerthree
          navigator={navigator} data={route.passProps} route={route}/>
      );
    }else{
      return (
        <Registerthree
        navigator={navigator} data={route.passProps} route={route}/>
      );
    }
    return this.noRoute(navigator);
  }

  noRoute(navigator) {
    return (
      <View style={{flex: 1, alignItems: 'stretch', justifyContent: 'center'}}>
        <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
            onPress={() => navigator.pop()}>
          <Text style={{color: 'red', fontWeight: 'bold'}}>请在 index.js 的 renderScene 中配置这个页面的路由</Text>
        </TouchableOpacity>
      </View>
    );
  }
 }

 const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
 });

 AppRegistry.registerComponent('Skyweg', () => Skyweg);
