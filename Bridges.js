/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage,
  Button,
  Image,
  KeyboardAvoidingView,
  Navigator,
  Picker,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View
} from 'react-native';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import LinearGradient from 'react-native-linear-gradient';

class Bridges extends Component {

  state = {
    email: '',
    username: '',
    firstname: '',
    lastname: '',
    gender: '',
    birthday: '',
    phone: '',
    address: '',
    image: '',
    token: '',
    bridge: '',
    bridgestatus: '',
    selectData: '',
    menuHeight: true,
    menuHeightSize: 0,
    temp: ''
  }

  componentDidMount() {
      FCM.requestPermissions(); // for iOS
      FCM.getFCMToken().then(async (fcm_token) => {
        try {
          AsyncStorage.getItem("myToken").then((_value) => {
            if (_value !== null) {
              this.setState({token: _value});
              this.setState({'token': _value});
            }
            try {

              let responsed = fetch(url + 'devices/', {
                method: 'GET',
                headers: {
                  'content-type': 'application/json',
                  'x-app-key': apiKey,
                  'Authorization': 'Token ' + _value
                }
              }).then((responsed) => {

                JSON.stringify('respuesta: ' + responsed);

                if (responsed.status == '404') {
                  let responsed = fetch(url + 'devices/', {
                    method: 'POST',
                    headers: {
                      'content-type': 'application/json',
                      'x-app-key': apiKey,
                      'Authorization': 'Token ' + _value
                    },
                    body: JSON.stringify({
                      'registration_id': fcm_token,
                      'device_id': _value,
                      'type': 'android'
                    })
                  }).then((responsed_) => {
                    console.warn('respuesta device: ' + JSON.stringify(responsed_));
                    return false;
                  })
                } else if (responsed.status == '200' || responsed.status == '201') {

                } else {

                }

                return false;
              })

            } catch(error){
              console.warn('error:-'+error);
            }
          });
        } catch (error) {
          console.warn('Error at login');
        }

    });

      this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
        // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
        if(notif.local_notification){
          console.warn('hay una notificacion local');
          //this is a local notification
        }
        if(notif.opened_from_tray){
          console.warn('hay una notificacion from tray');
          //app is open/resumed because user clicked banner
        }
        await someAsyncCall();

        if(Platform.OS ==='ios'){
          //optional
          //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
          //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
          //notif._notificationType is available for iOS platfrom
          switch(notif._notificationType){
            case NotificationType.Remote:
            notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
            break;
            case NotificationType.NotificationResponse:
            notif.finish();
            break;
            case NotificationType.WillPresent:
            notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
            break;
          }
        }
      });
      this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (fcm_token) => {
        AsyncStorage.getItem("myToken").then((_value) => {
          if (_value !== null) {
            this.setState({token: _value});
            this.setState({'token': _value});
          }

          let responsed = fetch(url + 'users/', {
            method: 'POST',
            headers: {
              'content-type': 'application/json',
              'x-app-key': apiKey,
              'Authorization': 'Token ' + _value
            }
          }).then((responsed_) => {
            let responsed = fetch(url + 'devices/' + responsed_.id + '/' , {
            method: 'PUT',
            headers: {
                      'content-type': 'application/json',
                      'x-app-key': apiKey,
                      'Authorization': 'Token ' + _value
                    },
                    body: JSON.stringify({
                    'registration_id': fcm_token,
                    'device_id': _value,
                    'type': 'android'
                  })
                }).then((responsed_) => {
                console.warn('respuesta device: ' + JSON.stringify(responsed_));
                return false;
              })
            return false;

          })

        });
        // fcm token may not be available on first load, catch it here
      });
  }

  constructor(props){
    super(props);
    try {
      AsyncStorage.getItem("myToken").then((_value) => {
        if (_value !== null) {
          this.setState({token: _value});

          try {
            let response = fetch(url + 'bridges/', {
              method: 'GET',
              headers: {
                'content-type': 'application/json',
                'x-app-key': apiKey,
                'Authorization': 'Token ' + _value
              }
            }).then((response) => {
              console.warn(JSON.stringify(response));
              console.warn(JSON.stringify(_value));

              var resAux = JSON.parse(response._bodyInit);
              this.setState({bridge:resAux[0].name, bridgestatus:resAux[0].status})

              if (response.status == 403) {
                JSON.stringify(response)
              } else if(response.status == 200 || response.status == 201) {

                let token = JSON.parse(response._bodyInit);
                return false;

              } else {
                JSON.stringify(response)

              }
            });

          } catch (e) {
            console.warn(e);
          }

        }
      }).then();
    } catch (error) {
      console.warn('Error at login');
    }

    try {
      let response = fetch('http://api.openweathermap.org/data/2.5/weather?id=4923517&units=metric&APPID=52e5e864401310b844f53eb9c13415f0', {
        method: 'GET',
        headers: {
          'content-type': 'application/json',
        }
      }).then((response) => {

        var resAux = JSON.parse(response._bodyInit);

        this.setState({temp: resAux.main.temp});

      });

    } catch (e) {
      console.warn(e);
    }
  }

  gotoSchedule = () => {
    try {

      let response = fetch('http://192.168.1.139:8000/' + 'users', {
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          'x-app-key': apiKey,
          'Authorization': 'Token ' + this.state.token
        }
      }).then((response) => {
          var resAux = JSON.parse(JSON.stringify(response));

          if (resAux.status == 403) {

          } else if(resAux.status == 200 || resAux.status == 201) {

            let token = JSON.parse(resAux._bodyInit);

            this.setState({
              id: token.id,
              email: token.email,
              username: token.username,
              firstname: token.first_name,
              lastname: token.last_name,
              gender: token.profile.gender,
              birthday: token.profile.birthday,
              phone: token.profile.phone,
              image: token.profile.image,
              address: token.profile.address
            });

            this.props.navigator.push({
              id: 'Schedule',
              passProps: {
                id: token.id,
                email: token.email,
                username: token.username,
                firstname: token.first_name,
                lastname: token.last_name,
                gender: token.profile.gender,
                birthday: token.profile.birthday,
                image: token.profile.image,
                token: this.state.token || this.props.data.token
              }
            });
            return false;
          }else{
            console.warn('error1');
            console.warn(JSON.stringify(response));
          }
      });

    } catch (e) {
      console.warn(e);
    }
  }

  gotoProfile = () => {
    try {
        let response = fetch(url + 'users', {
          method: 'GET',
          headers: {
            'content-type': 'application/json',
            'x-app-key': apiKey,
            'Authorization': 'Token ' + this.state.token
          }
        }).then((response) => {
          var resAux = JSON.parse(JSON.stringify(response));
          if (resAux.status == 403) {
            JSON.stringify(response);

          } else if(resAux.status == 200 || resAux.status == 201) {
            let token = JSON.parse(resAux._bodyInit);

            this.setState({
              id: token.id,
              email: token.email,
              username: token.username,
              firstname: token.first_name,
              lastname: token.last_name,
              gender: token.profile.gender,
              birthday: token.profile.birthday,
              phone: token.profile.phone,
              address: token.profile.address
            });
            this.props.navigator.push({
              id: 'Profile',
              passProps: {
                id: token.id,
                email: token.email,
                username: token.username,
                firstname: token.first_name,
                lastname: token.last_name,
                gender: token.profile.gender,
                birthday: token.profile.birthday,
                image: token.profile.image,
                token: this.state.token || this.props.data.token
              }
            });
            return false;
          }
      });

    } catch (e) {
      console.warn(e);
    }
  }

  gotoLogout = () => {
    AsyncStorage.removeItem('myToken');
    this.props.navigator.push({
      id: 'Login',
    });
  }

  configState = () => {
    if (this.state.menuHeight) {
      this.setState({menuHeightSize:120, menuHeight: !this.state.menuHeight});
    } else{
      this.setState({menuHeightSize:0, menuHeight: !this.state.menuHeight});
    }
  }

  render() {
    return (
      <Navigator
        renderScene={this.renderScene.bind(this)}
        navigator={this.props.navigator} />
    );
  };

  renderScene(route, navigator) {
    return (
      <View navigator={navigator} style={styles.home}>
        <LinearGradient colors={['#5055E4', '#FE8F5A']} style={styles.loginStyles}>
          <Image source={require('./images/palms.png')} style={{position: 'absolute'}}>
          </Image>

          <View style={{flex:1, flexDirection:'column', alignItems: 'flex-end'}}>
          <View style={{alignItems:'flex-end', paddingRight:50}}>
          <TouchableOpacity onPress={this.configState}>
          <Image source={require('./images/config.png')} />
          </TouchableOpacity>
          </View>
          <View style={{backgroundColor:'#FFF',
          elevation: 1,
          transform:[{translateX:-50}],
          shadowColor: '#F00',
          shadowOffset: { width: 10, height: 12 },
          shadowOpacity: 0.8,
          shadowRadius: 5,
          height: this.state.menuHeightSize
        }}>
        <TouchableOpacity onPress={this.gotoSchedule}>
        <Text style={{color:'#000', paddingRight:20, paddingLeft:60, paddingTop:10, paddingBottom:10, borderBottomWidth:1, borderColor:'#C3C3C3'}} onPress={this.gotoSchedule}>Schedule</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.gotoProfile}>
        <Text style={{color:'#000', paddingRight:20, paddingLeft:60, paddingTop:10, paddingBottom:10, borderBottomWidth:1, borderColor:'#C3C3C3'}} onPress={this.gotoProfile}>Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.gotoLogout}>
        <Text style={{color:'#000', paddingRight:20, paddingLeft:60, paddingTop:10, paddingBottom:10}} onPress={this.gotoLogout}>Logout</Text>
        </TouchableOpacity>
        </View>
          </View>

          <View style={{ flex:1 }}>
          <Text style={{textAlign:'center', color: 'white', fontSize:15}}>The Bridge</Text>
          <Text style={{textAlign:'center', color: 'white', fontSize:40, fontWeight:'bold', textShadowColor:'#010101', textShadowOffset:{width: 1, height:0}, textShadowRadius:0}}>
          {this.state.bridge}
          </Text>
          <Text style={{textAlign:'center', color: 'white', fontSize:15}}>is</Text>
          <Text style={{textAlign:'center', color: 'white', fontSize:50, fontWeight:'bold', textShadowColor:'#030303', textShadowOffset:{width: 0, height:1}, textShadowRadius:5, fontWeight:'bold'}}>
          {(this.state.bridgestatus? 'Open' : 'Closed'  )}
          </Text>
          </View>

          <View style={{marginBottom:70, flexDirection: 'row', flex:1, alignItems : 'center', justifyContent: 'center'}}>
            <View style={{maxWidth:170}}>
              <Image source={require('./images/cloudy.png')} style={{width:70, height:54, marginRight:20}}/>
              <Text style={{fontSize:10, textAlign:'center', color:'white'}}>Cloudy</Text>
            </View>

            <Image source={require('./images/divisor.png')} style = {{height: 120}} />

            <View style={{marginLeft:15}}>
              <Text style = {{fontWeight:'100', fontSize:30, color:'white', textAlign:'center'}}>{this.state.temp} °C</Text>
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  home:{
    flex:1,
    flexDirection:'column',
    justifyContent: 'space-around'
  },
  loginPage: {
    flex: 1
  },
  loginStyles: {
    flex:1,
    paddingTop: 10,
    alignItems:"stretch",
    justifyContent:"center"
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  wrapperTable: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex: 1
  },
  wrapperTabletTitle: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1
  },
  tableRight: {
    flex: 1,
    flexDirection: 'row',
    fontSize: 10,
    color: 'white',
    textAlign: 'right',
    paddingRight: 40
  },
  tableLeft: {
    flex: 2,
    flexDirection: 'row',
    fontSize: 20,
    color: 'white',
    textAlign: 'left'
  },
  title: {
    color: 'white',
    fontSize: 24
  },

  rightButton: {
    width: 100,
    height: 37,
    position: 'absolute',
    bottom: 8,
    right: 2,
    padding: 8
  }
});

module.exports = Bridges;
