/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  Alert,
  AppRegistry,
  StyleSheet,
  Button,
  KeyboardAvoidingView,
  Image,
  Navigator,
  Picker,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
  ScrollView

} from 'react-native';
import Menu, { MenuContext, MenuOptions, MenuOption, MenuTrigger } from 'react-native-menu';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import md5 from "react-native-md5";
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import LinearGradient from 'react-native-linear-gradient';

class Editprofile extends Component {
  state = {
    username: this.props.data.username,
    firstname: this.props.data.firstname,
    gender: this.props.data.gender,
    email: this.props.data.email,
    password: '',
    newpassword: false,
    image: (this.props.data.image? this.props.data.image : url+'media/skyweg-logo-yellow.png'),
    base64Icon: '',
    changed: false,
    thereisborder: 0,
    bordercolor: '#F00',
    birthday: this.props.data.birthday,
    token: this.props.data.token,
    saved: false,

    defusername : '',
    deffirstname : '',
    defemail : '',
    defimage : '',
    defbirthday : '',
    defgender : ''
  };


  saveChanges = () => {
    this.setState({
      defusername : this.state.username,
      deffirstname : this.state.firstname,
      defemail : this.state.email,
      defimage : this.state.image,
      defbirthday : this.state.birthday,
      defgender : this.state.gender,
      saved: true
    });

    this.setState({saved: false});

      if (this.state.newpassword) {

        if (this.state.bordercolor != '#F00') {

          let str_md5v = md5.str_md5( this.state.password + "" );

          if (this.state.changed ) {
            var body_ = JSON.stringify({
              "email": this.state.email,
              "username": this.state.username,
              "firstname": this.state.firstname,
              "password": str_md5v,
              "profile":{
                "gender": this.state.gender,
                "age": this.state.age,
                "image": 'data:image/jpeg;base64,'+this.state.base64Icon,
                'birthday' : this.state.birthday
              }
            });
          }else {
            console.warn('sin nueva foto');
            var body_ = JSON.stringify({
              "email": this.state.email,
              "username": this.state.username,
              "firstname": this.state.firstname,
              "password": str_md5v,
              "profile":{
                "gender": this.state.gender,
                "age": this.state.age,
                'birthday' : this.state.birthday
              }
            });
          }


        } else {
          Alert.alert('The passwords are not equal');
        }

      } else {

        if (this.state.changed) {
            console.warn('nueva foto');
            console.warn(this.state.base64Icon);

          var body_ = JSON.stringify({
            "email": this.state.email,
            "username": this.state.username,
            "firstname": this.state.firstname,

            "profile":{
              "gender": this.state.gender,
              "age": this.state.age,
              "image": 'data:image/jpeg;base64,'+this.state.base64Icon,
              'birthday' : this.state.birthday
            }
          });
        } else {
          var body_ = JSON.stringify({
            "email": this.state.email,
            "username": this.state.username,
            "firstname": this.state.firstname,

            "profile":{
              "gender": this.state.gender,
              "age": this.state.age,
              'birthday' : this.state.birthday
            }
          });
        }
      }

    try {
      console.warn(this.props.data.id);
      let response = fetch(url + 'users/' + this.props.data.id + '/', {
        method: 'PUT',
        headers: {
          'content-type': 'application/json',
          'x-app-key': apiKey,
          'Authorization': 'Token ' + this.state.token
        },
        body: body_
      }).then((response) => {
        var resAux = JSON.parse(JSON.stringify(response));
        console.warn(JSON.stringify(response));
        if (resAux.status == 403) {

        } else if(resAux.status == 200 || resAux.status == 201) {

          let token = JSON.parse(resAux._bodyInit);
          return false;

        }else{
          console.warn(JSON.stringify(response));
        }
      });
    } catch (e) {
      console.warn(e);
    }

  }

  gotoProfile = () => {

    if (this.state.saved) {

      this.props.navigator.push({
        id: 'Profile',
        passProps: {
          id: this.props.data.id,
          email: this.state.defemail,
          username: this.state.defusername,
          first_name: this.state.deffirstname,
          image: this.state.defimage,
          gender: this.state.defgender,
          birthday: this.state.defbirthday,
          token: this.state.token || this.props.data.token
        }
      });
      return false;
    } else {

      this.props.navigator.push({
        id: 'Profile',
        passProps: {
          id: this.props.data.id,
          email: this.state.email,
          username: this.state.username,
          firstname: this.state.firstname,
          lastname: this.state.lastname,
          image: this.state.image,
          gender: this.state.gender,
          birthday: this.state.birthday,
          token: this.state.token || this.props.data.token
        }
      });
      return false;
    }

  }

  gotoPrivacy = () => {

    if (this.state.saved) {

      this.props.navigator.push({
        id: 'Privacy',
        passProps: {
          id: this.props.data.id,
          email: this.state.defemail,
          username: this.state.defusername,
          first_name: this.state.deffirstname,
          last_name: this.state.lastname,
          image: this.state.defimage,
          gender: this.state.defgender,
          birthday: this.state.defbirthday,
          token: this.state.token || this.props.data.token
        }
      });
      return false;
    } else {

      this.props.navigator.push({
        id: 'Privacy',
        passProps: {
          id: this.props.data.id,
          email: this.state.email,
          username: this.state.username,
          first_name: this.state.firstname,
          last_name: this.state.lastname,
          image: this.state.image,
          gender: this.state.gender,
          birthday: this.state.birthday,
          token: this.state.token || this.props.data.token
        }
      });
      return false;
    }
  }

  getPicture = () => {
    var ImagePicker = require('react-native-image-picker');
    // Open Image Library:
    var options = {
      title: 'Select Avatar',
      /*customButtons: [
        {name: 'fb', title: 'Choose Photo from Facebook'},
      ],*/
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {

        this.setState({
          changed: true,
          avatarSource: response.uri,
          base64Icon: response.data
        });
      }
    });
  }
  /*
  <Text style={styles.title}>PRIVACY</Text>
  <View style={styles.group}>
    <TextInput style={styles.input} placeholder="         correo" value={this.props.data.email}/>
    <Text style={{fontSize:6, paddingLeft:15, paddingRight:15}}>Skyweg Report: The bridge on Brickell Ave. is now closed for a estimated time of 12 minutes</Text>
  </View>*/
componentDidMount() {}
componentWillUnmount() {}

render() {
  return (
    <Navigator
      renderScene={this.renderScene.bind(this)}
      navigator={this.props.navigator} />
    );
  };

  renderScene(route, navigator) {
    return (
      <ScrollView navigator={navigator} style={styles.home}>
        <View style={styles.header}>
          <LinearGradient colors={['#5055E4', '#964AF2']} style={{zIndex:999}} >
            <View style={{zIndex:999}}>
              <View style={{flex:1, flexDirection:'row', justifyContent:'space-around', alignItems: 'center', marginTop:15, marginBottom:25}}>
                <TouchableOpacity>
                  <Text style={{color:'#FFF', fontSize:20}} onPress={this.gotoProfile}>Back</Text>
                </TouchableOpacity>
                <Text style={{color:'#FFF', fontSize:20}}>Settings</Text>
                <TouchableOpacity>
                  <Text style={{color:'#FFF', fontSize:20}} onPress={this.saveChanges}>Save</Text>
                </TouchableOpacity>
              </View>
              <View style={{flex:1, justifyContent:'center', marginTop: 5, zIndex:999, paddingBottom: 15}}>
                <Text style={{textAlign:'left', flex:1, color:'white', fontSize:30, paddingLeft:180}}>{this.props.data.firstname}</Text>
                <Text style={{textAlign:'left', flex:1, color:'white', fontSize:20, paddingLeft:180}}>{this.props.data.username}</Text>
              </View>
            </View>
          </LinearGradient>
        </View>

        <View>

          <View style={{position:'absolute', justifyContent:'center', alignItems:'center'}}>
            <TouchableOpacity style={{ height:150, width:200, zIndex:999, transform:[{translateY: -65} ]}} onPress={this.getPicture}>
              <Image source={{uri: (this.state.changed? 'data:image/png;base64,'+this.state.base64Icon : this.state.image ) }} style={{ height:130, width:130, paddingRight:50, borderRadius: 150, zIndex:1, marginLeft:25 }}/>
              <Image source={require('./images/take-phot.png') } style={{ height:60, width:60, zIndex:20, right:40, bottom:10,  position:'absolute' }}/>
            </TouchableOpacity>
          </View>

          <View style={styles.containers}>
            <Text style={styles.titlef}>PERSONAL INFORMATION</Text>

            <View style={styles.viewInput}>
              <View style={{alignItems:'center'}}>
                <Image source={require('./images/personal.png')} />
              </View>
              <TextInput style={styles.input}
                underlineColorAndroid={'transparent'} placeholder="         Name"
                value={this.state.username}
                onChangeText={
                  (text)=>{ this.setState({username: text});}
                }
              />
            </View>

            <View style={styles.viewInput}>
              <View style={{alignItems:'center'}}>
                <Image source={require('./images/calendar.png')} />
              </View>
              <DatePicker
                style={styles.input}
                date={this.state.birthday}
                mode="date"
                placeholder="          Select your birthday"
                format="YYYY-MM-DD"
                minDate="1900-01-01"
                maxDate="2030-01-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                onDateChange={(date) => {this.setState({birthday: date}); }}
              />
            </View>

            <View style={styles.viewInput}>
              <View style={{alignItems:'center'}}>
                <Image source={require('./images/gender.png')} />
              </View>
              <Picker
                selectedValue={this.state.gender}
                style={styles.input}
                onValueChange={(itemValue, itemIndex) => this.setState({gender: itemValue}) }>
                <Picker.Item
                  style={{backgroundColor:'#FFF', opacity:0.3}}
                  label="Femenino" value="F"
                />
                <Picker.Item
                  style={{backgroundColor:'#FFF', opacity:0.3}}
                  label="Masculino" value="M"
                />
              </Picker>
            </View>



          <Text style={styles.title}>PRIVATE INFORMATION</Text>
          <View style={styles.group}>
          <TextInput style={styles.input} placeholder="         Primary Email" value={this.state.email}
          onChangeText={
            (text)=>{ this.setState({email: text});}
          }
          />
          </View>

          <Text style={styles.title}>CHANGE PASSWORD</Text>
          <View style={styles.group}>
          <TextInput style={styles.input} placeholder="         new password"
          onChangeText={
            (text)=>{
              if (text != '') {
                this.setState({newpassword: true, thereisborder: 1, password: text});
              } else {
                this.setState({newpassword: false, thereisborder: 0});
              }
            }
          }
          />
          <TextInput style={{flex:1, backgroundColor: '#fff', height: 30,padding: 0,fontSize: 15, borderWidth:this.state.thereisborder, borderColor: this.state.bordercolor}} placeholder="          confirm password"
          onChangeText={
            (text)=>{
              if (this.state.newpassword) {
                if(text == this.state.password){ this.setState({bordercolor:'green'}) } else {this.setState({bordercolor:'#F00'})};
              }
            }
          }
          />
          </View>

          <TouchableOpacity style={styles.privacy} onPress={this.gotoPrivacy}>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <Text style={{borderColor: '#FFF',  textAlign:'center', fontSize: 15}}>Privacy Policy  </Text>
              <Text> > </Text>
            </View>
          </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  };
}

const styles = StyleSheet.create({

  home:{
    flex:1,
    flexDirection:'column',
    backgroundColor: '#E5E5E5'
  },
  header:{
    zIndex:0,
    flex:2
  },
  containers:{
    flex:1,
    zIndex:0,
    justifyContent:'space-around',
    paddingTop: 30
  },
  title:{
    fontWeight:'bold',
    color: '#979797',
    paddingLeft: 15,
    marginTop: 40,
    marginBottom: 5,
    fontSize:12
  },
  titlef:{
    fontWeight:'bold',
    color: '#979797',
    paddingLeft: 15,
    marginTop: 50,
    marginBottom: 5,
    fontSize:12
  },
  viewInput:{
    paddingLeft:10,
    flex:1,
    backgroundColor:'#fff',
    flexDirection:'row',
    alignItems:'center',
    marginBottom:1
  },
  input:{
    flex:1,
    backgroundColor: '#fff',
    paddingLeft: 20,
    height:30
  },

  input:{
    flex:1,
    backgroundColor: '#fff',
    paddingLeft: 10
  },
  privacy:{
    backgroundColor:'#f3f3f3',
    marginTop: 20,
    marginBottom: 10,
    paddingTop: 10,
    paddingBottom: 10,
  }

});

module.exports = Editprofile;
