/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Button,
  KeyboardAvoidingView,
  Image,
  Navigator,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

class Home extends Component {
  state = {
    behavior: 'padding',
    modalOpen: false,
  };

  onSegmentChange = (segment: String) => {
    this.setState({behavior: segment.toLowerCase()});
  };

  render() {
    return (
      <Navigator renderScene={this.renderScene.bind(this)}
          navigator={this.props.navigator} />
    );
  };

  renderScene(route, navigator) {
    return (
      <View style={styles.home}>
        <LinearGradient colors={['#3A006C', '#FFB096']} style={styles.loginStyles}>

          <View style={{ flex: 1, flexDirection:'row', justifyContent:'space-around'}}>
            <Text style={{color:'white'}}>Back</Text>
            <Text style={{color:'white'}}>Create account</Text>
            <Text style={{color:'white'}}>Edit</Text>
          </View>

          <View style={{ justifyContent:'center', alignItems:'center'}}>
            <Text style={{justifyContent: 'flex-end', color:'white'}}>Choose your itinerary</Text>
          </View>

          <View style={{flex:1, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
            <View style={{justifyContent:'center', alignItems:'center'}}>
              <Image source={require('./images/Vector.png')} />
              <Text>Departure</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent:'space-between' }} >
              <TextInput
                keyboardType = 'numeric'
                maxLength = {2}
                style={{color:'white', fontSize:40}}
                underlineColorAndroid= 'transparent'
                defaultValue='00'
              />
            </View>
            <Text style={{  }} >:</Text>
            <View style={{flexDirection: 'row', justifyContent:'space-around' }} >
              <TextInput
                keyboardType = 'numeric'
                maxLength = {2}
                style={{color:'white', fontSize:40}}
                underlineColorAndroid= 'transparent'
                defaultValue='0'
              />
            </View>
          </View>

          <View style={{justifyContent:'center', alignItems:'center'}} >
            <Image source={require('./images/Vector.png')} />
          </View>

          <View style={{flex:1, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
            <View style={{justifyContent:'center', alignItems:'center'}}>
              <Image source={require('./images/Vector.png')} />
              <Text>Return</Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent:'space-around' }} >
              <TextInput
                keyboardType = 'numeric'
                maxLength = {2}
              />
            </View>
            <Text style={{  }} >:</Text>
            <View style={{flexDirection: 'row', justifyContent:'space-around' }} >
              <TextInput
                keyboardType = 'numeric'
                maxLength = {2}
              />
            </View>
          </View>


          <View style={{ flex: 1}}>
            <View style={{flex:1, justifyContent: 'center', alignItems: 'stretch'}}>
              <Text style={{color:'white', fontSize:16, fontWeight: 'bold', textAlign: 'right', paddingRight: 100}}>Alerts & Notifications</Text>
            </View>

            <View style={{flexDirection:'row', justifyContent: 'space-around'}}>
              <View>
                <Text style={{color:'white', fontSize:16, fontWeight: 'bold', borderRadius: 50 }}>M</Text>
                <Image source={require('./images/Vector.png')} />
              </View>

              <View>
                <Text style={{color:'white', fontSize:16, fontWeight: 'bold', borderRadius: 50 }}>T</Text>
                <Image source={require('./images/Vector.png')} />
              </View>

              <View>
                <Text style={{color:'white', fontSize:16, fontWeight: 'bold', borderRadius: 50 }}>W</Text>
                <Image source={require('./images/Vector.png')} />
              </View>

              <View>
                <Text style={{color:'white', fontSize:16, fontWeight: 'bold', borderRadius: 50 }}>T</Text>
                <Image source={require('./images/Vector.png')} />
              </View>

              <View>
                <Text style={{color:'white', fontSize:16, fontWeight: 'bold', borderRadius: 50 }}>F</Text>
                <Image source={require('./images/Vector.png')} />
              </View>

              <View>
                <Text style={{color:'white', fontSize:16, fontWeight: 'bold', borderRadius: 50 }}>S</Text>
                <Image source={require('./images/Vector.png')} />
              </View>

              <View>
                <Text style={{color:'white', fontSize:16, fontWeight: 'bold', borderRadius: 50 }}>S</Text>
                <Image source={require('./images/Vector.png')} />
              </View>

            </View>
          </View>
        </LinearGradient>
      </View>
    );
  };

  gotoRegister() {
    this.props.navigator.push({
      id: 'Register'
    });
    return false;
  };
}

const styles = StyleSheet.create({
  home:{
    flex:1,
    flexDirection:'column',
    justifyContent: 'space-around'
  },
  loginPage: {
    flex: 1
  },
  loginStyles: {
    flex:1,
    paddingTop: 10,
    alignItems:"stretch",
    justifyContent:"center"
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  wrapperTable: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex: 1
  },
  wrapperTabletTitle: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1
  },
  tableRight: {
    flex: 1,
    flexDirection: 'row',
    fontSize: 10,
    color: 'white',
    textAlign: 'right',
    paddingRight: 40
  },
  tableLeft: {
    flex: 2,
    flexDirection: 'row',
    fontSize: 20,
    color: 'white',
    textAlign: 'left'
  },
  title: {
    color: 'white',
    fontSize: 24
  }
});

module.exports = Home;
