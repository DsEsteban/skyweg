/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  Alert,
  AppRegistry,
  AsyncStorage,
  StyleSheet,
  Button,
  KeyboardAvoidingView,
  Image,
  Navigator,
  Picker,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View
} from 'react-native';
import Menu, { MenuContext, MenuOptions, MenuOption, MenuTrigger } from 'react-native-menu';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import DateTimePicker from 'react-native-modal-datetime-picker';

import LinearGradient from 'react-native-linear-gradient';

const API_KEY = "6325d9555b87c846c7d470a7bba232a7";

class Schedule extends Component {
  state = {
    scheduleCreated: false,
    isDateTimePickerVisible: false,
    behavior: 'padding',
    modalOpen: false,
    language: 'english',
    borderLun: 1,
    paddingRightLun: 6,

    borderMar: 1,
    paddingRightMar: 6,

    borderMir: 1,
    paddingRightMir: 6,

    borderJue: 1,
    paddingRightJue: 6,

    borderVie: 1,
    paddingRightVie: 6,

    borderSab: 1,
    paddingRightSab: 6,

    borderDom: 1,
    paddingRightDom: 6,

    activated: 'in',
    tokenId: 0,

    /* para el endpoint*/
    lunes:false,
    martes:false,
    miercoles:false,
    jueves:false,
    viernes:false,
    sabado:false,
    domingo:false,
    dateIn: '-- : --',
    dateOut: '-- : --',

  };

  constructor(props){
    super(props);
    AsyncStorage.getItem("myToken").then((_value) => {

      if (_value !== null) {
        this.setState({token: _value});
        this.setState({'token': _value});
      }
    }).then(() => {
      this.setState({loaded:true})
    });
  }

  componentWillMount() {

    try {
      let response = fetch(url + 'schedules/', {
        method: 'GET',
        headers: {
          'content-type': 'application/json',
          'x-app-key': apiKey,
          'Authorization': 'Token ' + this.props.data.token
        }
      }).then((response) => {
        var _body = JSON.parse(response._bodyInit);
        this.setState({tokenId: _body.id});
      })
    } catch(error){
      console.warn(error);
    }

  }

  componentDidMount() {
      FCM.requestPermissions(); // for iOS
      FCM.getFCMToken().then(async (token) => {
        try {
          let responsed = await fetch(url + 'devices/', {
            method: 'GET',
            headers: {
              'content-type': 'application/json',
              'x-app-key': apiKey,
              'Authorization': 'Token ' + this.state.token
            }
          }).then((responsed) => {
            var _body = JSON.parse(JSON.stringify(responsed._bodyInit));
          return false;
        })

      } catch(error){
        console.warn('error:-'+error);
      }
    });

      this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
        // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
        if(notif.local_notification){
          console.warn('hay una notificacion local');
          //this is a local notification
        }
        if(notif.opened_from_tray){
          console.warn('hay una notificacion from tray');
          //app is open/resumed because user clicked banner
        }
        await someAsyncCall();

        if(Platform.OS ==='ios'){
        //optional
        //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
        //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
        //notif._notificationType is available for iOS platfrom
        switch(notif._notificationType){
        case NotificationType.Remote:
        notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
        break;
        case NotificationType.NotificationResponse:
        notif.finish();
        break;
        case NotificationType.WillPresent:
        notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
        break;
      }
      }
      });
      this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
        console.warn('tknChanged ' + token)
        // fcm token may not be available on first load, catch it here
      });
  }

  gotoSchedule = () => {
    this.props.navigator.push({
      id: 'Schedule',
      passProps: {
              id: this.props.data.id,
              email: this.props.data.email,
              password: this.props.data.password,
              username: this.props.data.username,
              firstname: this.props.data.firstname,
              lastname: this.props.data.lastname,
              token: this.props.data.token,
              image: this.props.data.image,
              birthday:this.props.data.birthday,
              gender:this.props.data.gender
            }
    });
    return false;
  }

  gotoProfile = () => {
    this.props.navigator.push({
      id: 'Profile',
      passProps: {
        id: this.props.data.id,
        email: this.props.data.email,
        username: this.props.data.username,
        firstname: this.props.data.firstname,
        gender: this.props.data.gender,
        image: this.props.data.image,
        birthday: this.props.data.birthday,
        token: this.state.token || this.props.data.token
      }
    });
    return false;
  }

  saveSchedule = () => {

    var status = this.state.lunes || this.state.martes || this.state.miercoles ||
    this.state.jueves || this.state.viernes || this.state.sabado || this.state.domingo;
    console.warn(status);
    console.warn(this.state.dateIn);
    console.warn(this.state.dateOut);
    if (status && this.state.dateIn !== '-- : --' && this.state.dateOut !== '-- : --') {
      if (this.state.scheduleCreated) {
        try {
          let response = fetch(url + 'schedules/' + this.state.tokenId + '/', {
            method: 'PUT',
            headers: {
              'content-type': 'application/json',
              'x-app-key': apiKey,
              'Authorization': 'Token ' + this.props.data.token
            },
            body: JSON.stringify({
              'monday': this.state.lunes,
              'tuesday': this.state.martes,
              'wednesday': this.state.miercoles,
              'thursday': this.state.jueves,
              'friday': this.state.viernes,
              'saturday': this.state.sabado,
              'sunday': this.state.domingo,
              'hour_from': this.state.dateIn,
              'hour_until': this.state.dateOut
            })
          }).then((response) => {
            var resAux = JSON.parse(JSON.stringify(response));

            if (resAux.status == 403 || resAux.status == 404) {

            } else if(resAux.status == 200 || resAux.status == 201){

              Alert.alert('Saving schedule');

            } else {

            }
          })
        } catch(error){
          console.warn(error);
        }
      } else {
        try {
          let response = fetch(url + 'schedules/', {
            method: 'POST',
            headers: {
              'content-type': 'application/json',
              'x-app-key': apiKey,
              'Authorization': 'Token ' + this.props.data.token
            },
            body: JSON.stringify({
              'monday': this.state.lunes,
              'tuesday': this.state.martes,
              'wednesday': this.state.miercoles,
              'thursday': this.state.jueves,
              'friday': this.state.viernes,
              'saturday': this.state.sabado,
              'sunday': this.state.domingo,
              'hour_from': this.state.dateIn,
              'hour_until': this.state.dateOut
            })
          }).then((response) => {
            var resAux = JSON.parse(JSON.stringify(response));

            if (resAux.status == 403 || resAux.status == 404) {
              console.warn(JSON.stringify(response));

            } else if(resAux.status == 200 || resAux.status == 201){
              Alert.alert('Saving changes...');
              console.warn(JSON.stringify(response));

            } else {
              console.warn(JSON.stringify(response));
            }
          })
        } catch(error){
          console.warn(error);
        }
      }
    } else {
      Alert.alert('Please, choose an itinerary and the days of the week of your preference');
    }

  }

  changeBorder = (e, day : String) => {
    switch (day) {
      case 'lun':
      this.setState({borderLun: (this.state.borderLun == 1 ? 2 : 1 )});
      this.setState({paddingRightLun: (this.state.paddingRightLun == 6 ? 4 : 6 )});
        break;

      case 'mar':
        this.setState({borderMar: (this.state.borderMar == 1 ? 3 : 1 )});
        this.setState({paddingRightMar: (this.state.paddingRightMar == 6 ? 4 : 6 )});
        break;

      case 'mir':
        this.setState({borderMir: (this.state.borderMir == 1 ? 3 : 1 )});
        this.setState({paddingRightMir: (this.state.paddingRightMir == 6 ? 2 : 6 )});
        break;

      case 'jue':
        this.setState({borderJue: (this.state.borderJue == 1 ? 3 : 1 )});
        this.setState({paddingRightJue: (this.state.paddingRightJue == 6 ? 3 : 6 )});
        break;

      case 'vie':
        this.setState({borderVie: (this.state.borderVie == 1 ? 3 : 1 )});
        this.setState({paddingRightVie: (this.state.paddingRightVie == 6 ? 3 : 6 )});
        break;

      case 'sab':
        this.setState({borderSab: (this.state.borderSab == 1 ? 3 : 1 )});
        this.setState({paddingRightSab: (this.state.paddingRightSab == 6 ? 3 : 6 )});
        break;

      case 'dom':
        this.setState({borderDom: (this.state.borderDom == 1 ? 3 : 1 )});
        this.setState({paddingRightDom: (this.state.paddingRightDom == 6 ? 3 : 6 )});
        break;

      default:
        console.warn(JSON.stringify('error: try later'));
        break;
    }
  }

componentDidMount() {
  try {
    let response = fetch(url + 'schedules/', {
                           method: 'GET',
                           headers: {
                             'content-type': 'application/json',
                             'x-app-key': apiKey,
                             'Authorization': 'Token ' + this.props.data.token
                           }
                         }).then((response) => {

                           var resAux = JSON.parse(JSON.stringify(response));

                           if (resAux.status == 403 || resAux.status == 404) {
                             console.warn('no hay agregados');
                             console.warn(JSON.stringify(response));

                           } else if(resAux.status == 200 || resAux.status == 201){

                             let body = JSON.parse(resAux._bodyInit);
                            var timein = body.hour_from.split(':');
                            var timeout = body.hour_until.split(':');

                             this.setState({
                               scheduleCreated: true,
                               lunes:body.monday,
                               martes:body.tuesday,
                               miercoles:body.wednesday,
                               jueves:body.thursday,
                               viernes:body.friday,
                               sabado:body.saturday,
                               domingo:body.sunday,
                               dateIn: timein[0]+':'+timein[1],
                               dateOut: timeout[0]+':'+timeout[1],
                             })

                             if (body.monday) {
                               this.setState({borderLun:  2});
                               this.setState({paddingRightLun: 4});
                             }
                             if (body.tuesday) {
                               this.setState({borderMar:  3});
                               this.setState({paddingRightMar: 4});
                             }
                             if (body.wednesday) {
                               this.setState({borderMir:  3});
                               this.setState({paddingRightMir: 2});
                             }
                             if (body.thursday) {
                               this.setState({borderJue:  3});
                               this.setState({paddingRightJue: 3});
                             }
                             if (body.friday) {
                               this.setState({borderVie:  3});
                               this.setState({paddingRightVie: 3});
                             }
                             if (body.saturday) {
                               this.setState({borderSab:  3});
                               this.setState({paddingRightSab: 3});
                             }
                             if (body.sunday) {
                               this.setState({borderDom:  3});
                               this.setState({paddingRightDom: 3});
                             }

                           }else {
                             console.warn(JSON.stringify(response));
                           }
                         })
  } catch(error){
    console.warn(error);
  }
}

componentWillUnmount() {}

  _showDateTimePicker = (e, picked : String) => {this.setState({ isDateTimePickerVisible: true, activated: picked });}

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (_date) => {
    var _time = new Date(_date);
    time = _time.getHours() +  ':' + _time.getMinutes();

        if (this.state.activated == 'in') {
          this.setState({dateIn: time});
        }else{
          this.setState({dateOut: time});
        }
    this._hideDateTimePicker();
  };
/*
BackHandler.addEventListener('hardwareBackPress', function() {

  this.props.navigator.push({
    id: 'Schedule',
    passProps: {
            id: this.props.data.id,
            email: this.props.data.email,
            password: this.props.data.password,
            username: this.props.data.username,
            firstname: this.props.data.firstname,
            lastname: this.props.data.lastname,
            token: this.props.data.token,
            image: this.props.data.image,
            birthday:this.props.data.birthday,
            gender:this.props.data.gender,
            token: this.state.token || this.props.data.token
          }
  });
  return false;
});
*/

render() {
  return (
    <Navigator
      renderScene={this.renderScene.bind(this)}
      navigator={this.props.navigator} />
    );
  };
  renderScene(route, navigator) {
    return (
      <View navigator={navigator} style={styles.home}>
        <LinearGradient colors={['#5055E4', '#FE8F5A']} style={styles.loginStyles}>

        <View style={{flex:1, flexDirection:'row', justifyContent:"space-around"}}>
          <TouchableOpacity>
            <Text style={{color:'#FFF'}} onPress={this.gotoProfile}>Profile</Text>
          </TouchableOpacity>
          <Text style={{color:'#FFF'}}>Create account</Text>
          <TouchableOpacity>
            <Text style={{color:'#FFF'}} onPress={this.saveSchedule}>Save</Text>
          </TouchableOpacity>
        </View>

          <View style={{flex:1, alignItems:'center'}}>
            <Text style={{color:'white'}}>Choose your itinerary</Text>
          </View>

          <View style={{flex:2, alignItems:'center', justifyContent:'space-between'}}>
            <TouchableOpacity style={{flex:1}} onPress={(e) => this._showDateTimePicker(e, 'in')}>
              <View style={styles.wrapTime}>
                <Image source={require('./images/departure.png')} />
                <Text style={styles.time}>{this.state.dateIn}</Text>
              </View>
            </TouchableOpacity>

            <View style={{flex:1, flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
              <Image style={{alignItems:'center'}} source={require('./images/Vector.png')} />
            </View>

            <TouchableOpacity style={{flex:1, marginTop:10}} onPress={(e) => this._showDateTimePicker(e, 'out')}>
              <View style={styles.wrapTime}>
                <Image source={require('./images/return.png')}/>
                <Text style={styles.time}>{this.state.dateOut}</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
            <Text style={{color:'white'}}>Choose your days to activate alerts & Notifications</Text>
          </View>

          <View style={{flex:1, flexDirection:'row', justifyContent:'space-around'}}>
            <TouchableOpacity onPress = {(e) => { this.changeBorder(e, 'lun'); this.setState({'lunes': !this.state.lunes})}}>
              <Text style={{maxHeight: 25,
                            maxWidth: 50,
                            marginLeft: 5,
                            marginRight: 5,

                            paddingLeft: 8,
                            paddingRight: this.state.paddingRightLun,
                            paddingTop: 5,
                            paddingBottom: 8,

                            fontSize:10,
                            borderRadius:100,
                            borderWidth: this.state.borderLun,
                            borderColor:'#f3f3f3',
                            color:'#f3f3f3'}}>
                M</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {(e) => {this.changeBorder(e, 'mar'); this.setState({'martes': !this.state.martes})}}>
              <Text style={{maxHeight: 25,
                          maxWidth: 50,
                          marginLeft: 5,
                          marginRight: 5,

                          paddingLeft: 8,
                          paddingRight: this.state.paddingRightMar,
                          paddingTop: 5,
                          paddingBottom: 8,

                          fontSize:10,
                          borderRadius:100,
                          borderWidth: this.state.borderMar,
                          borderColor:'#f3f3f3',
                          color:'#f3f3f3'}}>
              T</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {(e) => {this.changeBorder(e, 'mir'); this.setState({'miercoles': !this.state.miercoles})}}>
              <Text style={{maxHeight: 25,
                          maxWidth: 50,
                          marginLeft: 5,
                          marginRight: 5,

                          paddingLeft: 8,
                          paddingRight: this.state.paddingRightMir,
                          paddingTop: 5,
                          paddingBottom: 8,

                          fontSize:10,
                          borderRadius:100,
                          borderWidth: this.state.borderMir,
                          borderColor:'#f3f3f3',
                          color:'#f3f3f3'}}>
              W</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {(e) => {this.changeBorder(e, 'jue'); this.setState({'jueves': !this.state.jueves})}}>
              <Text style={{maxHeight: 25,
                          maxWidth: 50,
                          marginLeft: 5,
                          marginRight: 5,

                          paddingLeft: 8,
                          paddingRight: this.state.paddingRightJue,
                          paddingTop: 5,
                          paddingBottom: 8,

                          fontSize:10,
                          borderRadius:100,
                          borderWidth: this.state.borderJue,
                          borderColor:'#f3f3f3',
                          color:'#f3f3f3'}}>
              T</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {(e) => {this.changeBorder(e, 'vie'); this.setState({'viernes': !this.state.viernes})}}>
              <Text style={{maxHeight: 25,
                          maxWidth: 50,
                          marginLeft: 5,
                          marginRight: 5,

                          paddingLeft: 8,
                          paddingRight: this.state.paddingRightVie,
                          paddingTop: 5,
                          paddingBottom: 8,

                          fontSize:10,
                          borderRadius:100,
                          borderWidth: this.state.borderVie,
                          borderColor:'#f3f3f3',
                          color:'#f3f3f3'}}>
              F</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {(e) => {this.changeBorder(e, 'sab'); this.setState({'sabado': !this.state.sabado})}}>
              <Text style={{maxHeight: 25,
                          maxWidth: 50,
                          marginLeft: 5,
                          marginRight: 5,

                          paddingLeft: 8,
                          paddingRight: this.state.paddingRightSab,
                          paddingTop: 5,
                          paddingBottom: 8,

                          fontSize:10,
                          borderRadius:100,
                          borderWidth: this.state.borderSab,
                          borderColor:'#f3f3f3',
                          color:'#f3f3f3'}}>
              S</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress = {(e) => {this.changeBorder(e, 'dom'); this.setState({'domingo': !this.state.domingo})}}>
              <Text style={{maxHeight: 25,
                          maxWidth: 50,
                          marginLeft: 5,
                          marginRight: 5,

                          paddingLeft: 8,
                          paddingRight: this.state.paddingRightDom,
                          paddingTop: 5,
                          paddingBottom: 8,

                          fontSize:10,
                          borderRadius:100,
                          borderWidth: this.state.borderDom,
                          borderColor:'#f3f3f3',
                          color:'#f3f3f3'}}>
              S</Text>
            </TouchableOpacity>
          </View>

        </LinearGradient>
        <DateTimePicker
          mode='time'
          is24Hour= {false}
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          onDateChange={(date) => {this.setState({date: date})}}
        />
      </View>
    );
  };
}

const styles = StyleSheet.create({
  home:{
    flex:1,
    flexDirection:'column',
    justifyContent: 'space-around'
  },
  loginStyles: {
    flex:1,
    flexDirection:'column',
    justifyContent:"space-around"
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {maxHeight: 25,
    maxWidth: 50,
    marginLeft: 5,
    marginRight: 5,

    paddingLeft: 8,
    paddingRight: 6,
    paddingTop: 5,
    paddingBottom: 8,

    fontSize:10,
    borderRadius:100,
    borderWidth:1,
    borderColor:'#f3f3f3',
    color:'#f3f3f3',
    fontSize: 20,
    textAlign: 'center',
  },
  wrapTime:{
    flex:1,
    flexDirection: 'row'
  },
  time:{
    marginLeft:10,
    fontSize:40,
    color:'white',
    justifyContent: 'space-around'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333'
  },
  title: {
    color: 'white',
    fontSize: 24
  },
  daysM:{
    maxHeight: 25,
    maxWidth: 50,
    marginLeft: 5,
    marginRight: 5,

    paddingLeft: 8,
    paddingRight: 6,
    paddingTop: 5,
    paddingBottom: 8,

    fontSize:10,
    borderRadius:100,
    borderWidth: 1,
    borderColor:'#f3f3f3',
    color:'#f3f3f3'
  },
  days:{
    maxHeight: 25,
    maxWidth: 50,
    marginLeft: 5,
    marginRight: 5,

    paddingLeft: 10,
    paddingRight: 8,
    borderWidth:1,
    paddingTop: 5,
    paddingBottom: 8,

    fontSize:10,
    borderRadius:100,
    borderColor:'#f3f3f3',
    color:'#f3f3f3'
  }
});

module.exports = Schedule;
