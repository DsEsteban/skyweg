/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Button,
  KeyboardAvoidingView,
  Image,
  Navigator,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

class Login extends Component {
  state = {
    behavior: 'padding',
    modalOpen: false,
  };

  onSegmentChange = (segment: String) => {
    this.setState({behavior: segment.toLowerCase()});
  };

  onForgotPass = () => {
    alert('amnesia1;')
  };

  render() {
    return (
      <Navigator
          renderScene={this.renderScene.bind(this)}
          navigator={this.props.navigator} />
    );
  };

  renderScene(route, navigator) {
    return (
      <View style={styles.home}>
        <LinearGradient colors={['#E94E2F', '#FFB096']} style={styles.loginStyles}>
          <KeyboardAvoidingView behavior={this.state.behavior}>
              <View style={{alignItems:'center'}}>
                <Image source={require('./images/skyweg-logo-yellow.png')} style={{width: 140, height: 140}} />
              </View>
              <TextInput
                onValueChange={this.onSegmentChange}
                selectedIndex={this.state.behavior === 'padding' ? 0 : 1}
                style={{height: 40, width: 250, marginBottom:10, borderWidth: 0, backgroundColor:'#FFF', opacity:0.3}}
                placeholder="         Email"
                underlineColorAndroid='rgba(0,0,0,0)'
              />

              <View style={{flexDirection: 'row'}}>
                <TextInput
                  style={{height: 40, flex:1, marginBottom:10, borderWidth: 0, backgroundColor:'#FFF', opacity:0.3}}
                  onValueChange={this.onSegmentChange}
                  selectedIndex={this.state.behavior === 'padding' ? 0 : 1}
                  secureTextEntry={true}
                  placeholder="         Password"
                  underlineColorAndroid='rgba(0,0,0,0)'
                />

                <View style={{flex:1,backgroundColor:'#FFF', opacity:0.3, height: 40, width: 100, paddingRight:15, justifyContent:'center'}}>
                  <Text onPress={this.onForgotPass} style={{textAlign:'right', color: '#EB7C5f'}}>Forget?</Text>
                </View>
              </View>

              <View style={{borderWidth:1.2,
                            paddingLeft: 20,
                            paddingRight: 20,
                            paddingTop: 2,
                            paddingBottom: 2,
                            borderRadius:15,
                            borderColor: '#FFF'}}>
                <TouchableHighlight>
                  <Text style={{borderColor: '#FFF',  textAlign:'center', fontSize: 15, color: '#FFF'}}>LOGIN</Text>
                </TouchableHighlight>
              </View>

              <TouchableHighlight onPress={this.gotoRegister.bind(this)}>
                <Text style={{marginTop: 10, textAlign:'center', fontSize: 12, color: '#FFF'}}>New User?
                  <Text style={{color: '#FF0'}}> Register here.</Text>
                </Text>
              </TouchableHighlight>
          </KeyboardAvoidingView>
          <View style={{flex:1}}>
            <Image source={require('./images/waves.png')} />
          </View>
        </LinearGradient>
      </View>
    );
  };

  gotoRegister() {
    this.props.navigator.push({
      id: 'Register'
    });
    return false;
  };
}

const styles = StyleSheet.create({
  home:{
    flex:1,
    flexDirection:'column',
    alignItems:'stretch'
  },
  loginPage: {
    flex: 1
  },
  loginStyles: {
    flex:1,
    paddingTop: 10,
    alignItems:"center",
    justifyContent:"center"
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});

module.exports = Login;
