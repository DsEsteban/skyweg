/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage,
  StyleSheet,
  Button,
  KeyboardAvoidingView,
  Image,
  Navigator,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const ACCESS_TOKEN = 'access_token';

class Register extends Component {
  state = {
    behavior: 'padding',
    username: "",
    email: "",
    secure: true,
    password: ""
  };

  async onRegisterPressed() {
    try {

      let response = fetch('http://testskyweg-dev.us-west-2.elasticbeanstalk.com/users', {
                              method: 'POST',
                              headers:{
                                "content-type": "application/json",
                                "authorization": "Token d57f5aaae686dc366d3364b4b337c7f6d1b774f5",
                                "cache-control": "no-cache"
                              },
                              body: JSON.stringify({
                               "email": "hr@thesocialus.com",
                               "username": "hr",
                               "password": "skyweg"
                              })
                            });
      let res = await response;
      console.warn('exito');
      console.warn(JSON.stringify(res));

    } catch(errors) {
      console.warn(errors);
      console.warn('hola');
    }
  }

  onSegmentChange = (segment: String) => {
    this.setState({behavior: segment.toLowerCase()});
  };

  onForgotPass = () => {
    this.setState({secure:!this.state.secure}, function(){
      if (this.state.secure)
        alert('Your pass is now hidden');
      else
        alert('Your pass is now visible. Be careful.');
    });
  }

  
  render() {
    return (
      <Navigator
        renderScene={this.renderScene.bind(this)}
        navigator={this.props.navigator}
      />
    )
  }

  renderScene(route, navigator) {
    return (
      <View style={styles.home}>
        <LinearGradient colors={['#EA5030', '#252EC2']} style={styles.loginStyles}>
          <KeyboardAvoidingView behavior={this.state.behavior}>
            <View style={{alignItems:'center'}}>
              <Image source={require('./images/skyweg-logo-yellow.png')} style={{width: 140, height: 140}} />
            </View>
            <TouchableHighlight style={{backgroundColor: '#ED5A53', marginBottom:10, alignItems:'center', justifyContent:'center'}} >
              <Text style={{marginTop: 10, fontSize: 12, color: '#FFF'}}>
                Connect with Google
              </Text>
            </TouchableHighlight>

            <View style={{flexDirection:'row', marginBottom:10, alignItems:'center', justifyContent:'center'}}>
              <Image source={require('./images/divisor.png')} />
              <Text style={{color:'#FFF'}}>   or   </Text>
              <Image source={require('./images/divisor.png')} />
            </View>

            <TouchableHighlight style={{flexDirection: 'row'}} onPress={this.gotoRegisterOne.bind(this)}>
              <View style={{flex:1, marginBottom:10,flexDirection: 'row', alignItems:'center', borderWidth: 0, backgroundColor:'#FFF', opacity:0.3, height:35}} onPress={this.gotoRegisterOne.bind(this)}>
                <Text>          Email</Text>
              </View>
            </TouchableHighlight>

            <TouchableHighlight style={{flexDirection: 'row'}} onPress={this.gotoRegisterOne.bind(this)}>
              <View style={{flex:1, flexDirection:'row', alignItems:'center', justifyContent:'space-between', marginBottom:10, borderWidth: 0, backgroundColor:'#FFF', opacity:0.3, height:35}}>
                <Text>          Password</Text>
                <Text style={{textAlign:'right', color: '#88DCCA'}}>show        </Text>
              </View>
            </TouchableHighlight>
          </KeyboardAvoidingView>

          <Text style={{fontSize:8, letterSpacing:0.4, fontFamily:'Roboto', textAlign:'center', paddingLeft:30, paddingRight:30}}>
            We may use your email for updates and tips on Skyweg products and services. You can unsubscribe for free at any time in your notification preferences. By signing in, you agree to out Terms of Use, Cookie Policy & Privacy Policy.
          </Text>
        </LinearGradient>
      </View>
    );
  };

  gotoRegisterOne() {
    this.props.navigator.push({
      id: 'Registerone'
    });
    return false;
  };
}

const styles = StyleSheet.create({
  home:{
    flex:1,
    flexDirection:'column',
    alignItems: 'stretch'
  },
  loginPage: {
    flex: 1
  },
  loginStyles: {
    flex:1,
    paddingTop: 10,
    alignItems:"center",
    justifyContent:"center"
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});

module.exports = Register;
