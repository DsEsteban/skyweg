/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  AsyncStorage,
  StyleSheet,
  Button,
  KeyboardAvoidingView,
  Image,
  Navigator,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const ACCESS_TOKEN = 'access_token';

class Register extends Component {
  state = {
    username: "",
    firstname: "",
    lastname: "",
    usernamebool: false,
    firstnamebool: false,
    lastnamebool: false,
  };

  async onRegisterPressed() {
    try {

      let response = fetch('http://testskyweg-dev.us-west-2.elasticbeanstalk.com/users', {
                              method: 'POST',
                              headers:{
                                "content-type": "application/json",
                                "authorization": "Token d57f5aaae686dc366d3364b4b337c7f6d1b774f5",
                                "cache-control": "no-cache"
                              },
                              body: JSON.stringify({
                               "email": "hr@thesocialus.com",
                               "username": "hr",
                               "password": "skyweg"
                              })
                            });
      let res = await response;
      console.warn('exito');
      console.warn(JSON.stringify(res));

    } catch(errors) {
      console.warn(errors);
      console.warn('hola');
    }
  }

  onSegmentChange = (segment: String) => {
    this.setState({behavior: segment.toLowerCase()});
  };

  enableNext = () => {
    if (this.state.usernamebool && this.state.firstnamebool && this.state.lastnamebool) {
      this.setState({next: '#fff'});
    } else{
      this.setState({next: '#6f6f6f'});
    }
    return false;
  }

  onForgotPass = () => {
    this.setState({secure:!this.state.secure}, function(){
      if (this.state.secure)
        alert('Your pass is now hidden');
      else
        alert('Your pass is now visible. Be careful.');
    });
  }

  checkPassToThree = () => {
    if (this.state.usernamebool && this.state.firstnamebool && this.state.lastnamebool) {
      this.props.navigator.push({
        id: 'RegisterThree',
        passProps: {
                email: this.props.data.email,
                password: this.props.data.password,
                username: this.state.username,
                firstname: this.state.firstname,
                lastname: this.state.lastname,
                age:'',
                gender:''
              }
      });
      return false;
    }
  }

  render() {
    return (
      <Navigator
        renderScene={this.renderScene.bind(this)}
        navigator={this.props.navigator}
      />
    )
  }

  renderScene(route, navigator) {
    return (
      <View navigator={navigator} style={styles.homeOne} >
        <LinearGradient colors={['#252EC2', '#EA5030']} style={styles.loginStyles_}>
          <View style={{flex:1}}>
            <View style={{flex:1, flexDirection:'row', justifyContent:'space-around', alignItems: 'center'}}>
              <Text style={{color:'#FFF'}}>Back</Text>
              <Text style={{color:'#FFF'}}>Create account</Text>
              <Text style={{color: this.state.next }} onPress={this.checkPassToThree}>Next</Text>
            </View>

            <View style={{flex:1, flexDirection:'row', justifyContent:'space-around', alignItems: 'center'}}>
              <Text style={{color:'#FFF'}}>h: {this.props.data.password}</Text>
              <Text style={{color:'#FFF'}}>h: {this.props.data.email}</Text>
            </View>

            <View style={{flex:1, flexDirection: 'row', justifyContent:'center'}}>
              <TextInput
                onChangeText={
                  (text)=>{
                    this.setState({username: text}, function(){ this.setState({usernamebool:(this.state.username == '' ? false : true)}, this.enableNext);
                  });
                }}
                style={{marginBottom:1, borderWidth: 0, width: 250, backgroundColor:'#FFF', opacity:0.3, height:35}}
                placeholder="         Username"
                autoFocus={true}
                underlineColorAndroid='rgba(0,0,0,0)'
              />
            </View>
            <View style={{flex:1, flexDirection: 'row', justifyContent:'center'}}>
              <TextInput
                onChangeText={
                  (text)=>{
                    this.setState({firstname: text}, function(){ this.setState({firstnamebool:(this.state.firstname == '' ? false : true)}, this.enableNext);
                  });
                }}
                style={{marginBottom:1, borderWidth: 0, width: 250, backgroundColor:'#FFF', opacity:0.3, height:35}}
                placeholder="         First Name"
                underlineColorAndroid='rgba(0,0,0,0)'
              />
            </View>
            <View style={{flex:4, flexDirection: 'row', justifyContent:'center'}}>
              <TextInput
                onChangeText={
                  (text)=>{
                    this.setState({lastname: text}, function(){ this.setState({lastnamebool:(this.state.lastname == '' ? false : true)}, this.enableNext);
                  });
                }}
                style={{marginBottom:1, borderWidth: 0, width: 250, backgroundColor:'#FFF', opacity:0.3, height:35}}
                placeholder="         Last Name"
                underlineColorAndroid='rgba(0,0,0,0)'
              />
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  homeOne:{
    flex:1,
    flexDirection:'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start'
  },
  loginStyles_: {
    flex:1,
    paddingTop: 10
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});

module.exports = Register;
