/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Button,
  KeyboardAvoidingView,
  Image,
  Navigator,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View
} from 'react-native';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';

import LinearGradient from 'react-native-linear-gradient';

class Profile extends Component {
  state = {
    behavior: 'padding',
    modalOpen: false,
  };

  onSegmentChange = (segment: String) => {
    this.setState({behavior: segment.toLowerCase()});
  };

  gotoBridges = () => {
    this.props.navigator.push({
      id: 'Bridges',
      passProps: {
              id: this.props.data.id,
              email: this.props.data.email,
              password: this.props.data.password,
              username: this.props.data.username,
              firstname: this.props.data.firstname,
              lastname: this.props.data.lastname,
              token: this.props.data.token,
              image: this.props.data.image,
              birthday:this.props.data.birthday,
              gender:this.props.data.gender
            }
    });
    return false;
  }

  gotoEditProfile = () => {
    this.props.navigator.push({
      id: 'Editprofile',
      passProps: {
              id: this.props.data.id,
              email: this.props.data.email,
              password: this.props.data.password,
              username: this.props.data.username,
              firstname: this.props.data.firstname,
              lastname: this.props.data.lastname,
              token: this.props.data.token,
              image: this.props.data.image,
              birthday:this.props.data.birthday,
              gender:this.props.data.gender
            }
    });
    return false;
  }
  componentDidMount() {
    this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
        // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
        if(notif.local_notification){
          console.warn('hay una notificacion local');
          //this is a local notification
        }
        if(notif.opened_from_tray){
          console.warn('hay una notificacion from tray');
          //app is open/resumed because user clicked banner
        }
        await someAsyncCall();

        if(Platform.OS ==='ios'){
          //optional
          //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
          //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
          //notif._notificationType is available for iOS platfrom
          switch(notif._notificationType){
            case NotificationType.Remote:
              notif.finish(RemoteNotificationResult.NewData) //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
              break;
            case NotificationType.NotificationResponse:
              notif.finish();
              break;
            case NotificationType.WillPresent:
              notif.finish(WillPresentNotificationResult.All) //other types available: WillPresentNotificationResult.None
              break;
          }
        }
    });
    this.refreshTokenListener = FCM.on(FCMEvent.RefreshToken, (token) => {
        console.warn('tkn ' + token)
        // fcm token may not be available on first load, catch it here
    });
  }

componentWillUnmount() {
    // stop listening for events
    this.notificationListener.remove();
    this.refreshTokenListener.remove();
}

  render() {
    return (
      <Navigator
        renderScene={this.renderScene.bind(this)}
        navigator={this.props.navigator} />
    );
  };

  renderScene(route, navigator) {
    return (
      <View navigator={navigator} style={styles.home}>
        <LinearGradient colors={['#9B51E0', '#2F80ED']} style={styles.loginStyles}>

          <View style={{ flex: 1}}>
            <View style={{flex:1, flexDirection:'row', justifyContent:'space-around', alignItems: 'center'}}>
              <TouchableOpacity>
                <Text style={{color:'#FFF'}} onPress={this.gotoBridges}>Back</Text>
              </TouchableOpacity>

              <TouchableOpacity>
                <Text style={{color:'#FFF'}} onPress={this.gotoEditProfile}>Edit</Text>
              </TouchableOpacity>
            </View>

            <View display='none' style={{alignItems:'center'}}>
              <Image source={{uri: (this.props.data.image? this.props.data.image : url+'media/skyweg-logo-yellow.png')}} style={{width: 70, height: 70, borderRadius:50}} />
            </View>
          </View>

          <View style={{flex:1, justifyContent: 'center', alignItems: 'stretch'}}>

            <View style={styles.wrapperTabletTitle}>
              <Text style={styles.title}> {this.props.data.username} </Text>
              <Text style={styles.title}> | </Text>
              <Text style={styles.title}> {this.props.data.birthday? this.props.data.birthday : 'Birthday'}</Text>
            </View>

            <View style={styles.wrapperTable}>
              <Text style={styles.tableRight}>NAME</Text>
              <Text style={styles.tableLeft}>{this.props.data.firstname}</Text>
            </View>

            <View style={styles.wrapperTable}>
              <Text style={styles.tableRight}>GENDER</Text>
              <Text style={styles.tableLeft}>{this.props.data.gender=='M'? 'Masculino': 'Femenino'}</Text>
            </View>

            <View style={styles.wrapperTable}>
              <Text style={styles.tableRight}>EMAIL</Text>
              <Text style={styles.tableLeft}>{this.props.data.email}</Text>
            </View>

          </View>

          <View style={{ flex: 1}}>

            <View style={{flex:1, justifyContent: 'center'}}>
              <Image source={require('./images/waves.png')} style={{maxHeight: 250, alignItems: 'stretch', justifyContent: 'flex-end'}}/>
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  home:{
    flex:1,
    flexDirection:'column',
    justifyContent: 'space-between'
  },
  loginPage: {
    flex: 1
  },
  loginStyles: {
    flex:1,
    paddingTop: 10,
    alignItems:"stretch",
    justifyContent:"center"
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  wrapperTable: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    flex: 1
  },
  wrapperTabletTitle: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1
  },
  tableRight: {
    flex: 1,
    flexDirection: 'row',
    fontSize: 10,
    color: 'white',
    textAlign: 'right',
    paddingRight: 40
  },
  tableLeft: {
    flex: 2,
    flexDirection: 'row',
    fontSize: 20,
    color: 'white',
    textAlign: 'left'
  },
  title: {
    color: 'white',
    fontSize: 24
  }
});

module.exports = Profile;
